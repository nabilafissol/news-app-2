import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:news_app/list_item.dart';
import 'package:http/http.dart' as http;

import 'entity/news.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Top Health Headlines in Malaysia'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Article> _newsList = new List();
  
  void getData() async {
    http.Response response = await http.get(
        "http://newsapi.org/v2/top-headlines?country=my&category=health&apiKey=a34ed7e329fb4ba8a2614651fec953e8");
    setState(() {
      _newsList = News.fromJson(json.decode(response.body)).articles;
    });
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
          child: ListView.builder(
        itemCount: _newsList.length,
        itemBuilder: (context, index) => ListItem(_newsList[index]),
      )),
    );
  }
}
